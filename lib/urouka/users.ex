defmodule(UserRegistered, do: defstruct([:name, :email, :pwd_hash]))

defmodule Urouka.Users do
  import SbResult

  def authenticate(user) do
    read_user_stream(user) >>>
      map(get_pasword_hash()) >>>
      map(validate_hash(user["password"]))
  end

  def read_user_stream(user) do
    args = %{repo_put: fn x -> IO.puts("putting end") end, repo_get: fn -> [] end}
    {:ok, pid} = SbEventstore.StreamSupervisor.find_or_create_process(user["email"], args)
    SbEventstore.Stream.get(user["email"])
    |> case do
      x when x == [] -> {:error, "User not found"}
      x -> {:ok, x}
    end
  end

  defp get_pasword_hash(events) do
    events
    |> Enum.reduce("", fn x, acc ->
      case x do
        %UserRegistered{} -> x.pwd_hash
        # User updadet
        _ -> acc
      end
    end)
  end

  def validate_hash(hash, password) do
    Argon2.verify_pass(password, hash)
    |> case do
      true -> {:ok, "authenticated"}
      false -> {:error, "wrong username/ password"}
    end
  end

  def validate_email(user) do
    case user["email"] do
      nil ->
        {:error, "Can not be nil"}

      x ->
        if String.contains?(x, "@") do
          {:ok, user}
        else
          {:error, "Must contain @"}
        end
    end
  end

  def validate_pwd(user) do
    case user["password"] do
      nil ->
        {:error, "Password cant be nil"}

      x ->
        if String.length(x) > 3 do
          {:ok, user}
        else
          {:error, "Password must be more than 8 characters"}
        end
    end
  end

  ## This must be checked inside eventstore process
  def assert_user_is_unique(user) do
    user
    |> read_user_stream()
    |> case do
      {:error, "User not found"} -> {:ok, user}
      {:ok, x} -> {:error, "User already exists"}
    end
  end

  def create_event(user) do
    {:ok,
     %UserRegistered{
       email: user["email"],
       name: user["name"],
       pwd_hash: Argon2.hash_pwd_salt(user["password"])
     }}
  end

  def store_event(created_event) do
    args = %{repo_put: fn x -> IO.puts("putting end") end, repo_get: fn -> [] end}
    "users"
    {:ok, pid} =
    "users"
      |> SbEventstore.StreamSupervisor.find_or_create_process(args)
    :ok = SbEventstore.Stream.append(created_event.email, created_event)

    {:ok, pid} =
      created_event.email
      |> SbEventstore.StreamSupervisor.find_or_create_process(args)
    :ok = SbEventstore.Stream.append(created_event.email, created_event)


  end
end
