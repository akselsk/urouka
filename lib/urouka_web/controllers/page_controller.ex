defmodule UroukaWeb.PageController do
  use UroukaWeb, :controller
  alias Phoenix.LiveView

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
