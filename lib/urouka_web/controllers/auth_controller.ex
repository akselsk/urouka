defmodule UroukaWeb.AuthController do
  use UroukaWeb, :controller

  @doc """
  This action is reached via `/auth/:provider` and redirects to the OAuth2 provider
  based on the chosen strategy.
  """
  def index(conn, %{}) do
    redirect(conn, external: authorize_url!())
  end

  def delete(conn, _params) do
    conn
    |> put_flash(:info, "You have been logged out!")
    |> configure_session(drop: true)
    |> redirect(to: "/")
  end

  @doc """
  This action is reached via `/auth/:provider/callback` is the the callback URL that
  the OAuth2 provider will redirect the user back to with a `code` that will
  be used to request an access token. The access token will then be used to
  access protected resources on behalf of the user.
  """
  def callback(conn, params) do
    code = params["code"]
    client = get_token!(code: code)
    user = OAuth2.Client.get!(client, "https://api.ouraring.com/v1/userinfo").body

    conn
    |> put_session(:current_user, user)
    |> put_session(:access_token, client.token.access_token)
    |> redirect(to: "/team_dashboard")
  end

  defp client do
    OAuth2.Client.new(
      # default
      strategy: OAuth2.Strategy.AuthCode,
      client_id: System.get_env("OURA_CLIENT"),
      client_secret: System.get_env("OURA_SECRET"),
      redirect_uri: "http://localhost:4000/auth/callback",
      state: "XXX",
      authorize_url: "https://cloud.ouraring.com/oauth/authorize",
      token_url: "https://api.ouraring.com/oauth/token"
    )
    |> OAuth2.Client.put_serializer("application/json", Jason)
  end

  def authorize_url! do
    OAuth2.Client.authorize_url!(client())
  end

  # you can pass options to the underlying http library via `opts` parameter
  def get_token!(params \\ [], headers \\ [], opts \\ []) do
    OAuth2.Client.get_token!(client(), params, headers, opts)
  end

  # Strategy Callbacks

  def authorize_url(client, params) do
    OAuth2.Strategy.AuthCode.authorize_url(client, params)
  end

  # def get_token(client, params, headers) do
    # client
    # |> put_header("accept", "application/json")
    # |> OAuth2.Strategy.AuthCode.get_token(params, headers)
  # end

end
