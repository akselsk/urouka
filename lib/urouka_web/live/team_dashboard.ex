defmodule UroukaWeb.TeamDashboardLive do
  use Phoenix.LiveView
  import SbResult

  def mount(session, socket) do
    # Get team from socket
    {:ok, assign(socket, user: %{"email" => "", "password" => ""}, info: nil, danger: nil)}
  end

def handle_event("redirect_to_profile", _value, socket) do
    IO.inspect "hei"
    {:noreply, socket |> live_redirect(to: "/profile")}
end

  def render(assigns) do
    ~L"""
       <%= live_link to: "/profile" do %>
	<div class="barcontainer material-icons">
	menu
	</div>
       <% end %>
      <p class="alert alert-danger" role="alert"><%= @danger%></p>
      <p class="alert alert-info" role="alert"><%= @info%></p>
    <h4> Urouka</h4>
    <p> You have no teams, go to the profile page to
    connect your Oura account and set up a team </p>
    """
  end
end
