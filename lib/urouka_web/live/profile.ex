defmodule UroukaWeb.ProfileLive do
  use Phoenix.LiveView
  import SbResult

  def mount(session, socket) do
    # Get team from socket
    IO.inspect session
    {:ok, assign(socket, user: session.current_user)}
  end

  def render(assigns) do
    ~L"""
       <%= live_link to: "/team_dashboard" do %>
	<div class="barcontainer material-icons">
	close
	</div>
       <% end %>
    <h4> Profile </h4>
    <p> Hi <%= @user["email"] %> </p>
    <button> Connect to oura </button>
    """
  end
end
