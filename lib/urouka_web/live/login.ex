defmodule UroukaWeb.LoginLive do
  use Phoenix.LiveView
  import UroukaWeb.ErrorHelpers
  import SbResult

  def mount(_session, socket) do
    {:ok, assign(socket, user: %{"email" => "", "password" => ""}, info: nil, danger: nil)}
  end

  ## This can probably be a html file

  # def render(assigns), do: UroukaWeb.PageView.render("register.html",assigns)

  def handle_event("login", login_form, socket) do
    user = login_form["user"]

    user
    |> Urouka.Users.authenticate()
    |> case do
      {:error, reason} ->
        {:noreply, socket |> assign(user: user, danger: reason)}

      {:ok, x} ->
        IO.inspect(user["email"])

        {:stop,
         socket
         # A bit hacking
         |> put_flash(:user_id, user["email"])
         |> redirect(to: "/team_dashboard")}
    end
  end

  def render(assigns) do
    ~L"""
      <p class="alert alert-danger" role="alert"><%= @danger%></p>
      <p class="alert alert-info" role="alert"><%= @info%></p>
    <h4> Login </h4>
    <form  phx-submit="login">
    <input value="<%= @user["email"]%>" type="email" name="user[email]" placeholder=Email:  phx-debounce="blur"/>
    <input type="password" name="user[password]" placeholder=Password  phx-debounce="blur"/>
    <input type="submit" value="Submit" >
    </form>
    """
  end
end
