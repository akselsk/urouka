defmodule UroukaWeb.RegisterLive do
  use Phoenix.LiveView
  import UroukaWeb.ErrorHelpers
  import SbResult
  import Urouka.Users

  def mount(_session, socket) do
    {:ok,
     assign(socket,
       user: %{"email" => "", "password" => "", "confirm_password" => ""},
       errors: [],
       danger: nil,
       info: nil
     )}
  end

  # def render(assigns), do: UroukaWeb.PageView.render("register.html",assigns)

  def handle_event("create", registerform, socket) do
    ## Check logic here, ie, can not be any other created events
    registerform["user"]
    |> validate_email() >>>
      validate_pwd() >>>
      create_event >>>
      store_event
    |> case do
      :ok ->
        {:noreply,
         socket
         # A bit hacking
         |> put_flash(:info, "User sucessfully created")
         |> live_redirect(to: "/login")}

      {:error, reason} ->
        {:noreply, socket |> assign(danger: reason)}
    end
  end

  def handle_event("validate", registerform, socket) do
    errors = Map.fetch!(socket, :assigns) |> Map.fetch!(:errors)
    user = registerform["user"]

    updated_error =
      validate_email(user)
      |> case do
        {:ok, _x} -> errors |> Keyword.delete(:email)
        {:error, reason} -> errors |> Keyword.put(:email, reason)
      end

    ## Check if email is in use here
    new_error =
      validate_pwd(user)
      |> case do
        {:ok, _x} ->
          with_pwd_error = updated_error |> Keyword.delete(:password)

          unless user["password"] == user["confirm_password"] do
            Keyword.put(with_pwd_error, :confirm_password, "Not equal")
          end

        {:error, reason} ->
          updated_error
          |> Keyword.put(:password, reason)
          |> Keyword.delete(:confirm_password, reason)
      end
      |> case do
        nil -> []
        x -> x
      end

    {:noreply, assign(socket, user: user, errors: new_error)}
  end

  def render(assigns) do
    ~L"""
      <p class="alert alert-danger" role="alert"><%= @danger%></p>
      <p class="alert alert-info" role="alert"><%= @info%></p>
      <h4> register </h4>
    <form phx-change="validate" phx-submit="create">
    <input type="email" name="user[email]" <%= if String.length(@user["email"]) > 1 do "value= #{@user["email"]} "end %> placeholder=Email:  phx-debounce="blur"/>
    <%= error_tag2  @errors, :email %>
    <input type="password" name="user[password]" <%= if String.length(@user["password"]) > 1 do "value= #{@user["password"]} "end %> placeholder=Password  phx-debounce="blur"/>
    <%= if String.length(@user["password"]) > 1 do error_tag2  @errors, :password end %>
    <input type="password" name="user[confirm_password]" <%= if String.length(@user["confirm_password"]) > 1 do "value= #{@user["confirm_password"]} "end %> placeholder="Confirm password" phx-debounce="blur"/>
    <%= if String.length(@user["confirm_password"]) > 1 do error_tag2  @errors, :confirm_password end %>
    <input type="submit" value="Submit" <%= if @errors != [] do "disabled=true" end %>  >
    </form>
    """
  end
end
