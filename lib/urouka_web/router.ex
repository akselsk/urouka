defmodule UroukaWeb.Router do
  use UroukaWeb, :router
  use Phoenix.Router
  import Phoenix.LiveView.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug Phoenix.LiveView.Flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", UroukaWeb do
    #Use these in other apps.
    #pipe_through :browser
    #live "/register", RegisterLive, session: %{}
    #live "/login", LoginLive, session: %{}
  end

  scope "/auth", UroukaWeb do
    pipe_through :browser
    get "/", AuthController, :index
    get "/callback", AuthController, :callback
    delete "/logout", AuthController, :delete
  end

  scope "/", UroukaWeb do
    pipe_through [:browser]
    #UroukaWeb.Plugs.Auth
    live "/team_dashboard", TeamDashboardLive, session: [:current_user]
    live "/profile", ProfileLive, session: [:current_user]
    # libe
    # get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  # scope "/api", UroukaWeb do
  #   pipe_through :api
  # end
end
