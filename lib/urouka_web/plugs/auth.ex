defmodule UroukaWeb.Plugs.Auth do
  import Plug.Conn
  import Phoenix.Controller

  alias Urouka.Users

  def init(opts), do: opts

  def call(conn, _opts) do
    #new_conn = set_from_flash(conn)
    conn
    |> get_session(:current_user)
    |> case do
      nil ->
        conn
        |> redirect(to: "/auth")
        |> halt()
      user->
        conn
    end
  end


## Use in apps where i need a custom login
  #defp set_from_flash(conn) do
    #get_flash(conn, :user_id)
    #|> case do
      #nil ->
        #conn
#
      #user_id ->
        #conn
        #|> put_session(:user_id, user_id)
    #end
  #end
end
