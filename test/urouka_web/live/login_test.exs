defmodule UroukaWeb.LoginTest do
  use Phoenix.ConnTest
  use UroukaWeb.ConnCase
  import Phoenix.LiveViewTest
  @endpoint UroukaWeb.Endpoint

  test "Login user sucessfull", %{conn: conn} do
    {:ok, view, html} = live(conn, "/login")
    assert html =~ ""

    events = [
      %UserRegistered{
        email: "t@",
        name: nil,
        pwd_hash:
          "$argon2id$v=19$m=131072,t=8,p=4$saITF29W9V3T0/Xd8O+C6A$kef3fiaJF2ckIBc3HU+HETH2FPRv9oPXp6huk/B2/6Y"
      }
    ]

    args = %{repo_put: fn x -> IO.puts("putting end") end, repo_get: fn -> events end}
    {:ok, pid} = SbEventstore.StreamSupervisor.find_or_create_process("@t", args)

    assert_redirect(view, "/register", fn ->
      assert render_submit(view, :login, %{
               user: %{"password" => "123456790:", "email" => "t@"}
             })
    end)
  end
end
