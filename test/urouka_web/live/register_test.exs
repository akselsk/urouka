defmodule UroukaWeb.RegisterTest do
  use Phoenix.ConnTest
  use UroukaWeb.ConnCase
  import Phoenix.LiveViewTest
  @endpoint UroukaWeb.Endpoint

  test "Live register validation", %{conn: conn} do
    {:ok, view, html} = live(conn, "/register")
    assert html =~ ""

    assert render_change(view, :validate, %{
             user: %{"password" => "", "confirm_password" => "", "email" => "samma"}
           }) =~ "Must contain @"

    assert render_change(view, :validate, %{
             user: %{"password" => "12", "confirm_password" => "", "email" => "samma@"}
           }) =~ "Password must be more than 8 character"

    assert render_change(view, :validate, %{
             user: %{"password" => "123456790:", "confirm_password" => "12", "email" => "samma@"}
           }) =~ "Not equal"
  end

  test "Create and store user", %{conn: conn} do
    {:ok, view, html} = live(conn, "/register")
    assert html =~ ""

    render_submit(view, :create, %{
      user: %{"password" => "123456790:", "confirm_password" => "123456790", "email" => "t@"}
    })

    events = SbEventstore.Stream.get("t@")

    assert events = [
             %UserRegistered{
               email: "t@",
               name: nil,
               pwd_hash:
                 "$argon2id$v=19$m=131072,t=8,p=4$saITF29W9V3T0/Xd8O+C6A$kef3fiaJF2ckIBc3HU+HETH2FPRv9oPXp6huk/B2/6Y"
             }
           ]
  end
end
